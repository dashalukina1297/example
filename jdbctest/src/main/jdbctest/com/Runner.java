package com;

import com.config.SpringConfig;

import com.models.Student;
import com.repo.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class Runner {
    @Autowired
    private static StudentRepository studentRepository;

    public Runner(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }


    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new
                AnnotationConfigApplicationContext(SpringConfig.class);
        Student student = new Student(1L, "Ilya", "Sidorov", "Petrovish", 3.0);
        studentRepository.save(student);

        context.close();
    }

}
